Pre Test Assessment by a company in Malaysia

- Book Listing Layout. (ListView derived from JSON data) ---> completed
- Click the book will pop out dialog (ClickListener and Custom Dialog (book-info.png) -----> completed
- All button (Delete, Share, Read) are clickable and have a function to show information for each of them. You can test them deeply. ----> completed
- Model Dialog will close when the user taps the back button -----> completed
- The design is for 320px (width) and 480px (height). I choose 480px height because, when I make the research on the internet, and found a combination of 320px width with what height. It showed 480px. ----> completed
- All images in the folder you sent me are fully used. ----> completed

P/S : I am sorry for adding some value into the JSON file. Not intend to change the objective to the pre test. I just want to add something as plus point. Never intend.
